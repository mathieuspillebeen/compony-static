function trapFocus(element, focusableSelector) {
  if(element.classList.contains('js-trapped')) {

  } else {
    element.classList.add('js-trapped');

    element.addEventListener('keydown', function trappy(e) {
      var KEYCODE_TAB = 9;

      var focusableEls = element.querySelectorAll(focusableSelector);
      var firstFocusableEl = focusableEls[0];
      var lastFocusableEl = focusableEls[focusableEls.length - 1];
      console.log(focusableEls);

      var isTabPressed = (e.key === 'Tab' || e.keyCode === KEYCODE_TAB);

      if (!isTabPressed) {
        return;
      }

      /* shift + tab */
      if (e.shiftKey) {
        if (document.activeElement === firstFocusableEl) {
          lastFocusableEl.focus();
          e.preventDefault();
        }
      } /* tab */
      else {
        if (document.activeElement === lastFocusableEl) {
          firstFocusableEl.focus();
          e.preventDefault();
        }
      }
    });
  }
}
