/**
 * @file
 * Navigate to the correct url when a checkbox changes.
 */

(function ($, Drupal) {

  'use strict';

  Drupal.facets = Drupal.facets || {};
  Drupal.behaviors.facetsCheckboxWidget = {
    attach: function (context) {
      // Find all checkbox facet results and add an event listener.
      var $checkboxes = $('.js-facets-checkbox-links input:checkbox', context);
      $checkboxes.once('facets-checkbox-transform').each(Drupal.facets.addCheckboxChangeListener);
    }
  };

  /**
   * Add event listener to the change event of the checkboxes to navigate to the correct url.
   */
  Drupal.facets.addCheckboxChangeListener = function () {
    var $checkbox = $(this);
    $checkbox.on('change.facets', function (e) {
      Drupal.facets.disableCheckboxFacet($checkbox.parents('.js-facets-checkbox-links'));
      window.location.href = $(this).val();
    });
  };

  /**
   * Disable all facet checkboxes in the facet and apply a 'disabled' class.
   *
   * @param {object} $facet
   *   jQuery object of the facet.
   */
   Drupal.facets.disableCheckboxFacet = function ($facet) {
     $facet.addClass('facets-disabled form-disabled');
     var $checkboxes = $('.js-facets-checkbox-links input:checkbox');
     $checkboxes.click(Drupal.facets.preventDefault);
     $checkboxes.attr('disabled', true);
  };

  /**
   * Event listener for easy prevention of event propagation.
   *
   * @param {object} e
   *   Event.
   */
  Drupal.facets.preventDefault = function (e) {
    e.preventDefault();
  };

})(jQuery, Drupal);
