/**
 * @file
 * Navigate to the correct url when a dropdown changes.
 */

(function ($) {

  'use strict';

  Drupal.facets = Drupal.facets || {};
  Drupal.behaviors.facetsDropdownWidget = {
    attach: function (context, settings) {
      // Find all dropdown facets and add an event listener.
      $('select.js-facets-dropdown-links').once('facets-dropdown-change').each(Drupal.facets.addDropdownChangeListener);
    }
  };

  /**
   * Add event listener to the change event of the dropdown to navigate to the correct url.
   *
   * @param {object} context
   *   Context.
   * @param {object} settings
   *   Settings.
   */
  Drupal.facets.addDropdownChangeListener = function (context, settings) {
     // Go to the selected option when it's clicked.
    $(this).on('change', function () {
      window.location.href = $(this).val();
    });
  };

})(jQuery);
